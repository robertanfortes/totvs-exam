var express = require('express');
var app = express();

app.use(express.static(__dirname + "/public"));


var index = require('./routes/index');
app.use('/', index);

app.listen(3000);
console.log("server running on 3000");