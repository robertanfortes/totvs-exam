(function() {
'use strict';
var storageService = angular.module('totvs.storageService', []);
  storageService.factory('getLocalStorage', function () {
    var taskList = {};
    return {
      list: taskList,
      updateTask: function (TasksArr) {
        if (window.localStorage && TasksArr) {
            //Local Storage to add Data
            localStorage.setItem("todos", angular.toJson(TasksArr));
        }
        taskList = TasksArr;

      },
      getTask: function () {
        //Get data from Local Storage
        taskList = angular.fromJson(localStorage.getItem("todos"));
        return taskList ? taskList : [];
      }
    };
  });
})();
