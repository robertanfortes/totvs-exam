(function () {
'use strict';
angular.module('totvs.tasksList')
	.controller('TaskCtrl', ['$scope', 'getLocalStorage','$state','$rootScope','$filter', function($scope,getLocalStorage,$state,$rootScope,$filter) {
		// read task list from LocalStorage
    $scope.saved = getLocalStorage.getTask();
    $scope.todos = getLocalStorage.getTask();

    // Count task list
    $scope.count = $scope.todos.length;

		var dfinish = $scope.todoDate;
		var dstarte = new Date();
		$scope.sortType = 'dfinish'; // set the default sort type


    $scope.sortReverse  = false;  // set the default sort order
		$scope.searchTask   = ''; // set the default search/filter term
		//$scope.date = $filter("date")(Date.now(), 'yyyy-MM-dd')
		$scope.data = {
		 availableOptions: [
			 {id: '1', status: 'A iniciar'},
			 {id: '2', status: 'Iniciado'},
			 {id: '3', status: 'Em Review'},
			 {id: '4', status: 'Concluído'}

		 ],
		 selectedOption: {id: '1', status: 'A iniciar'} //This sets the default value of the select in the ui
		 };
		 var taskStatus = $scope.data.selectedOption.status;

		// add task e save localstorage
		$scope.addTodo = function() {

			$scope.todos.push({
				name: $scope.todoText,
				dfinish: $scope.todoDate,
				progress: $scope.data.selectedOption.status,
				done: false
			});

			$scope.todoText = ''; //clear the input after adding
			$scope.todoDate = ''; //clear the input after adding
			$scope.todoStatus = ''; //clear the input after adding

			getLocalStorage.updateTask($scope.todos);

		};
		// end add task

		// update and save task
		$scope.editingData = {};

		$scope.update = function(item, newName, newDate, NewSelectedOption){

			item.name = newName;
			item.dfinish = newDate;

			if (NewSelectedOption.status) {
				item.progress = NewSelectedOption.status;
				if (NewSelectedOption.status === "Concluído") {
					item.done = true;
				} else {
					item.done = false;
				}
			}
			getLocalStorage.updateTask($scope.todos);
	  };

			// inicio remover task
		$scope.removeTask = function(index) {

	        	$scope.todos.splice(index, 1);
				getLocalStorage.updateTask($scope.todos);

	   		 };
		 // fim remover task

		 // cont completed task
		$scope.remaining = function() {
			var count = 0;
			angular.forEach($scope.todos, function(todo){
				count+= todo.done ? 0 : 1;
			});
			return count;
		};
		// end cont completed task

		//  remove completed task
		$scope.archive = function() {
			var oldTodos = $scope.todos;
			$scope.todos = [];
			angular.forEach(oldTodos, function(todo){
				if (!todo.done)
					$scope.todos.push(todo);
			});
			getLocalStorage.updateTask($scope.todos);
		};
		// end remove completed task
	}]);
})();
