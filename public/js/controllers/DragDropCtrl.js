(function () {
'use strict';
angular.module('totvs.taskDrag')
  .controller('DragDropCtrl',['getLocalStorage', function($scope, getLocalStorage) {
         $scope.removeTask = function(index) {
      $scope.todos.splice(index, 1);
    };
  }]);
})();
