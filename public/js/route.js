(function () {

  angular.module('totvs.route', ['ui.router'])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {

  $stateProvider

     .state('index', {
      url: '/',
      templateUrl: 'partials/task.html',
      controller:  'TaskCtrl'
    })

    .state('table', {
     url: '/table',
     templateUrl: 'partials/task-table.html',
     controller:  'TaskCtrl'
   });


    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);
  });
})();
