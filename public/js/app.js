(function () {
    angular.module('totvs', [

			                      'totvs.tasksList',
			                      'totvs.route',
			                      'totvs.storageService',
			                      'totvs.taskDrag',
                            'totvs.taskTable'
    ]);

})();
