(function () {
'use strict';
angular.module('totvs.taskTable')
  .directive('tableTask', function() {
    return {
        restrict: 'E',
        replace:'true',
        templateUrl: '../partials/task-table.html'
    };
  });
  
})();
