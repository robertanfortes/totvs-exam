# Code challenge front end Totvs #

* Projeto desenvolvido em AngularJS para controle de atividades 

### Funcionalidades ###
* CRUD de atividade
* Buscar atividade com filtro
* Operação de deletar com drag and drop

### Working tools ###
* AngularJS
* npm
* Node server
* Bootstrap
* Git
* Atom

### How to build ###
* npm install
* node server

### Bugfix ###
* buscar por data - bug quando aplica filtro no campo data
